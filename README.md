# WITDOM MySQL Storage

WITDOM MySQL Storage provides a Dockerfile that builds upon the [official MySQL Docker image](https://hub.docker.com/r/library/mysql/). For testing and development purposes, this repository includes a SQL script *risk-scoring.sql*, that is used to initialize the database when the Docker container is started.

## Starting the database server
Before you can start the MySQL server you first have to build its Docker image:

````bash
$ docker build -t witdom-mysql-storage .
````

Then, to actually start the container, you have several options depending on:

* whether you want to connect to the MySQL database server from a client running in another Docker container, or from a client running on the Docker host
* whether you want to use the default values for MySQL-related configuration (database name, user, password, root password) or provide your own values.

You will use `docker run` command to start the container, however the arguments passed to it will differ based on the above choices.

#### MySQL server with default configuration

The basic way to start the MySQL server is like this:

````bash
$ docker run --name witdom-mysql-storage -p 3306:3306 witdom-mysql-storage
````
 
The above command will create:
* a single database, **witdom_financial** initialized with the data from the script *risk-scoring.sql*,
* a database user with superuser privileges (username **witdomuser**, password **witdomuserpw**),
* a **root** user with password **witdomadminpw**.

The above command will produce output similar to the one below:

    Initializing database
    Database initialized
    MySQL init process in progress...
    Warning: Unable to load '/usr/share/zoneinfo/Factory' as time zone. Skipping it.
    Warning: Unable to load '/usr/share/zoneinfo/iso3166.tab' as time zone. Skipping it.
    Warning: Unable to load '/usr/share/zoneinfo/leap-seconds.list' as time zone. Skipping it.
    Warning: Unable to load '/usr/share/zoneinfo/posix/Factory' as time zone. Skipping it.
    Warning: Unable to load '/usr/share/zoneinfo/right/Factory' as time zone. Skipping it.
    Warning: Unable to load '/usr/share/zoneinfo/zone.tab' as time zone. Skipping it.
    mysql: [Warning] Using a password on the command line interface can be insecure.
    mysql: [Warning] Using a password on the command line interface can be insecure.
    mysql: [Warning] Using a password on the command line interface can be insecure.
    mysql: [Warning] Using a password on the command line interface can be insecure.

    /usr/local/bin/docker-entrypoint.sh: running /docker-entrypoint-initdb.d/risk-scoring.sql
    mysql: [Warning] Using a password on the command line interface can be insecure.

    MySQL init process done. Ready for start up.

After that, the database server will be accessible at *127.0.0.1:3306*. Any client running on the Docker host will be able to access it. For example, we can test it with the mysql command line client running on the Docker host:

````
$ mysql witdom_financial -h 127.0.0.1 --user=witdomuser --password=witdomuserpw

...
Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show tables;
+----------------------------+
| Tables_in_witdom_financial |
+----------------------------+
| risk_scoring               |
| risk_scoring_es            |
+----------------------------+
2 rows in set (0,00 sec)
````

However, if you want to connect to the MySQL database server from a mysql client running in another docker container, you can omit passing the `-p 3306:3306` to the `docker run` command, and instead link the MySQL database container to container where the mysql client is running. For instance, let's assume that we want to contact the MySQL server from a container *my-container*. If we start the container like this:

````bash
$ docker run --name my-container -p :5000:5000 -t --link witdom-mysql-storage:mysql my-image
````

we can connect to the database server from within *my-container* by connecting to the host *mysql* and port *3306* (note the _link_ flag).

#### MySQL server with your own configuration (overriding defaults)
If you want to initialize your database server with other configuration (for instance, different user and password), you can override the defaults stated in the provided Dockerfile by passing your own values for the configuration you want to change. 

For example:

````bash
$ docker run --name witdom-mysql-storage \
 -e MYSQL_USER=myuser \
 -e MYSQL_PASSWORD=mypassword \
 -p 3306:3306 \
 witdom-mysql-storage
````

In the example above, only the MYSQL_USER and MYSQL_PASSWORD will override the defaults. Default values will still be used for the environment variables that were not explicitly passed to the `docker run` command.

> **Note**: Changing environment variable MYSQL_DATABASE to something other than *witdom_financial* will result in your database not being initialized with the data from the script *risk-scoring.sql*. If you want to change the database name, you will have to 1.) pass your own database name for the MYSQL_DATABASE variable and 2.) correct the database name at the beginning of the *risk-scoring.sql* script.

Again, if you want to connect to the MySQL server not from the Docker host, but from another Docker container, you can omit the port mapping and use container linking instead (see the example in the previous section). 

## Cleaning up

To stop and remove MySQL server container, run 

````bash
$ docker stop witdom-mysql-storage && docker rm witdom-mysql-storage
````

## Helper Makefile
**If you prefer**, you can use the Makefile provided in this repository to speed up building, starting and cleaning up MySQL server container (it uses default values and exposes port 3306 to the docker host). It has several targets, including *build*, *run*, *clean* and *logs*.

To build the Docker image and start docker container, run `make` (this is the same as if we ran a sequence of `make clean`, `make build` and `make run` commands).

To clean up (stop and remove container), run `make clean`.

## Additional information
For more information please refer to [the official MySQL Docker image documentation](
    https://hub.docker.com/r/library/mysql/).
