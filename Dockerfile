FROM mysql:5.7.14

ENV MYSQL_DATABASE=witdom_financial \
	MYSQL_USER=witdomuser \
	MYSQL_PASSWORD=witdomuserpw \
	MYSQL_ROOT_PASSWORD=witdomadminpw

ADD risk-scoring.sql /docker-entrypoint-initdb.d

EXPOSE 3306