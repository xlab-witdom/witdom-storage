IMAGE_NAME=witdom-mysql-storage
CONTAINER_NAME=witdom-mysql
#$(IMAGE_NAME)

all: clean build run

clean:
	-docker stop $(CONTAINER_NAME) && \
	docker rm $(CONTAINER_NAME)

build:
	docker build -t $(IMAGE_NAME) .

run:
	docker run --name $(CONTAINER_NAME) -p 3306:3306 $(IMAGE_NAME)

logs:
	docker logs $(CONTAINER_NAME)